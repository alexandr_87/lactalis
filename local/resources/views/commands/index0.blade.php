<!DOCTYPE html>
<html lang="en">
<head>
</head>
<body id="app-layout">
      <table border="1">
        <tr>
                <th>#</th>
                <th>Nume regiune</th>
                <th>Localitate</th>
                <th>Autor</th>
                <th>Data</th>
                <th>Cantitate</th>
          </tr>
        <tbody>
        @foreach($commands as $command)
          <tr>
              <td>{!!$command->id!!}</td>
              <td>{!!$command->locality->region->name!!}</td>
              <td>{!!$command->locality->name!!}</td>
              <td>
        	    	{!!$command->user->name!!}
        	    </td>
              <td>{!!$command->date!!}</td>
              <td>{!!$command->quantity!!}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
</body>
<html>
