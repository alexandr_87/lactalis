@extends('layouts.app')

@section('content')
  <div class="container">
    @if(Auth::check())
    @if(Auth::user()->role->name=="admin")
    		<div class="col-md-12">
                <div class="input-group" id="adv-search">
                    <input type="text" class="form-control" placeholder="filtre pentru cautare" disabled/>
                    <div class="input-group-btn">
                        <div class="btn-group" role="group">
                            <div class="dropdown dropdown-lg">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                                <div class="dropdown-menu dropdown-menu-right" role="menu">
                                      {!! Form::open(['class'=>'form-horizontal','id'=>'search_form']) !!}
                                        {!! Form::token() !!}
                                        <div class="form-group">
                                          <label for="filter">Regiune</label>
                                          <select name="regions" class="form-control">
                                            <option value="all" selected>All</option>
                                            @foreach($regions as $region)
                                              <option value="{!!$region->id!!}">{!!$region->name!!}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                        <div class="form-group">
                                          <label for="filter">Utilizator</label>
                                          <select id="users" name="users" class="form-control">
                                            <option value="all">All</option>
                                            @foreach($users as $user)
                                              <option value="{!!$user->id!!}">
                                                {!!$user->name!!}
                                              </option>
                                            @endforeach
                                          </select>
                                        </div>
                                        <div class="form-group">
                                          <label for="filter">Data inceput</label>
                                          <input class="form-control" type="date" data-date="" data-date-format="yyyy MMMM dd" id="date_start" name="date_start">
                                        </div>
                                        <div class="form-group">
                                          <label for="filter">Data sfirsit</label>
                                          <input class="form-control" type="date" data-date="" data-date-format="yyyy MMMM dd" id="date_end" name="date_end">
                                        </div>
                                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                    {!! Form::close()!!}
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                            {!! HTML::link('export', 'Export excel',['id'=>'btn_export','class' => 'btn btn-info']) !!}
                        </div>
                    </div>
                </div>
              </div>
        @endif
      <h2>Lista de comenzi</h2>
      <div class="row">
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <h3 class="panel-title">comenzi</h3>
                <div class="pull-right">
                    <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filtru</button>
                </div>
            </div>
      <table class="table table-condensed" id="commands" style="border-collapse:collapse;">
        <thead>
            <tr class="filters">
                <th><input type="text" class="form-control" placeholder="#" disabled></th>
                <th><input type="text" class="form-control" placeholder="Nume regiune" disabled></th>
                <th><input type="text" class="form-control" placeholder="Localitate" disabled></th>
                <th><input type="text" class="form-control" placeholder="Autor" disabled></th>
                <th><input type="text" class="form-control" placeholder="Data" disabled></th>
                <th><input type="text" class="form-control" placeholder="Cant" disabled></th>
              </tr>
          </thead>
        <tbody>
          <?php $i=0;?>
        @foreach($commands as $command)

          <tr data-toggle="collapse" data-target="#demo<?php echo $i;?>" class="accordion-toggle">
              <td>{!!$command->id!!}</td>
              <td>{!!$command->locality->region->name!!}</td>
              <td>{!!$command->locality->name!!}</td>
              <td>
        	    	{!!$command->user->name!!}
        	    </td>
              <td>{!!date("d-m-Y",strtotime($command->date))!!}</td>
              <td>{!!$command->quantity!!} L</td>
            </tr>
            <tr>
              <td colspan="6" class="hiddenRow">
                <div id="demo<?php echo $i;?>" class="accordian-body collapse">
                  <a href='delete/{!!$command->id!!}' id='btn_delete' class= 'btn btn-danger btn-xs'><span class="glyphicon glyphicon-trash"></span></a>
                </div>
              </td>
            </tr>
            <?php $i++;?>
          @endforeach
        </tbody>
      </table>
    </DIV>
      <div class="col-md-12">
          {!! $commands->links() !!}
      </div>
    </div>
    @else
        Pentru a vizualiza trebuie sa va logati
    @endif
  </div>

@endsection
