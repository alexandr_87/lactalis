@extends('layouts.app')

@section('content')
      <div class="container">
           <strong>
     				Adauga comanda:
            <br>
     			</strong>

        		{!! Form::open(['url'=>'save','method'=>'PUT','class'=>'ui form','files'=>true]) !!}

        	    <div class="field">
        	    	{!! Form::label('region', 'Alegeti raionul') !!}
                <select name="regions" class="form-control" id="regions">
                  @foreach($regions as $region)
              		  <option value="{!!$region->id!!}">{!!$region->name!!}</option>
                  @endforeach
              	</select>
        	    </div>

              <div class="field">
        	    	{!! Form::label('locality', 'Alegeti localitatea') !!}
                <select name="localities" class="form-control" id="localities">

              	</select>
        	    </div>

        	    <div class="field">
        	    	{!! Form::label('quantity', 'Introduceti cantitatea de lapte in litri') !!}
        	    	{!! Form::text('quantity', 0,['class'=>'form-control']) !!}
        	    </div>

              <div class="field">
        	    	{!! Form::label('locality', 'Alegeti cauza') !!}
                <select name="types" class="form-control">
                  @foreach($types as $type)
              		  <option value="{!!$type->id!!}">{!!$type->name!!}</option>
                  @endforeach
              	</select>
        	    </div>

        	    <div class="field">
        	    	Author:{!!Auth::user()->name!!}
        	    </div>

        	    <div class="field">
        	    	{!! Form::submit('Salveaza', ['class' => 'btn btn-success']) !!}
        	    </div>

        		{!! Form::close() !!}
        </div>
@endsection
