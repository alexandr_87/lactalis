@extends('layouts.app')

@section('content')
      <div class="container">
           <strong>
     				Adauga regiune:
            <br></br>
     			</strong>

        		{!! Form::open(['url'=>'save_region','method'=>'PUT','class'=>'ui form','files'=>true]) !!}

        	    <div class="field">
        	    	{!! Form::label('name', 'Introduceti nume regiune') !!}
        	    	{!! Form::text('name',null,['class'=>'form-control']) !!}
        	    </div>
              </br>
        	    <div class="field">
        	    	{!! Form::submit('Salveaza', ['class' => 'btn btn-success']) !!}
        	    </div>

        		{!! Form::close() !!}
        </div>
@endsection
