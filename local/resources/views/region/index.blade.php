@extends('layouts.app')

@section('content')
  <div class="container">
    @if(Auth::check())
      <h2>Lista de regiuni</h2>
      <a href="{!!URL::to('add_region')!!}" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-plus"></span> Adauga</a>
      <div class="row">
          <div class="panel panel-primary filterable">
              <div class="panel-heading">
                  <h3 class="panel-title">Regiuni</h3>
                  <div class="pull-right">
                      <button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filtru</button>
                  </div>
              </div>
              <table class="table">
                  <thead>
                      <tr class="filters">
                          <th><input type="text" class="form-control" placeholder="#" disabled></th>
                          <th><input type="text" class="form-control" placeholder="Nume regiune" disabled></th>
                          <th>Delete</th>
                      </tr>
                  </thead>
                  <tbody>
                    @foreach($regions as $region)
                      <tr>
                          <td>{!!$region->id!!}</td>
                          <td>{!!$region->name!!}</td>
                          <td>
                            <a href='delete_region/{!!$region->id!!}' id='btn_delete' class= 'btn btn-danger btn-xs'><span class="glyphicon glyphicon-trash"></span></a>
                          </td>
                      </tr>
                    @endforeach
                  </tbody>
              </table>
          </div>
          <div class="col-md-12">
		          {!! $regions->links() !!}
		      </div>
      </div>
    @else
        Pentru a vizualiza trebuie sa va logati
    @endif
  </div>

@endsection
