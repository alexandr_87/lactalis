@extends('layouts.app')

@section('content')
      <div class="container">
           <strong>
     				Adauga regiune:
            <br></br>
     			</strong>

        		{!! Form::open(['url'=>'save_locality','method'=>'PUT','class'=>'ui form','files'=>true]) !!}

        	    <div class="field">
        	    	{!! Form::label('name', 'Introduceti nume regiune') !!}
        	    	{!! Form::text('name',null,['class'=>'form-control']) !!}
        	    </div>
              </br>
              <div class="field">
                {!! Form::label('region', 'Alegeti raionul') !!}
                <select name="regions" class="form-control" id="regions">
                  @foreach($regions as $region)
                    <option value="{!!$region->id!!}">{!!$region->name!!}</option>
                  @endforeach
                </select>
              </div>
              <br>
        	    <div class="field">
        	    	{!! Form::submit('Salveaza', ['class' => 'btn btn-success']) !!}
        	    </div>

        		{!! Form::close() !!}
        </div>
@endsection
