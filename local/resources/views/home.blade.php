@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                <a href="list">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                       <div class="info-box">
                         <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
                         <div class="info-box-content">
                           <span class="info-box-text">Comenzi</span>
                           <span class="info-box-number">{!!$commands!!}</span>
                         </div><!-- /.info-box-content -->
                       </div><!-- /.info-box -->
                    </div><!-- /.col -->
                </a>
                <a href="list_users">
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Utilizatori</span>
                        <span class="info-box-number">{!!$users!!}</span>
                      </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                  </div><!-- /.col -->
                </a>
                <a href="list_regions">
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-yellow"><i class="ion-map"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Regiuni</span>
                        <span class="info-box-number">{!!$regions!!}</span>
                      </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                  </div><!-- /.col -->
                </a>
                <a href="list_localities">
                  <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                      <span class="info-box-icon bg-red"><i class="ion-person-stalker"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Localitati</span>
                        <span class="info-box-number">{!!$localities!!}</span>
                      </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                  </div><!-- /.col -->
                </a>
              </div>
              </div>
            </div>
        </div>
    </div>
@endsection
