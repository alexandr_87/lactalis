@extends('layouts.app')

@section('content')
      <div class="container">
           <strong>
     				Adauga utilizator:
            <br></br>
     			</strong>

        		{!! Form::open(['url'=>'save_user','method'=>'PUT','class'=>'ui form','files'=>true]) !!}

        	    <div class="field">
        	    	{!! Form::label('name', 'Introduceti nume utilizator') !!}
        	    	{!! Form::text('name',null,['class'=>'form-control']) !!}
        	    </div>
              </br>
              <div class="field">
                {!! Form::label('password', 'Introduceti parola') !!}
                {!! Form::text('password',null,['class'=>'form-control']) !!}
              </div>
              <br>
              <div class="field">
                {!! Form::label('region', 'Alegeti rolul') !!}
                <select name="roles" class="form-control" id="regions">
                  @foreach($roles as $role)
                    <option value="{!!$role->id!!}">{!!$role->name!!}</option>
                  @endforeach
                </select>
              </div>
              <br>
        	    <div class="field">
        	    	{!! Form::submit('Salveaza', ['class' => 'btn btn-success']) !!}
        	    </div>

        		{!! Form::close() !!}
        </div>
@endsection
