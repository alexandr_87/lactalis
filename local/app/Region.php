<?php

namespace App;
use App\Locality;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table="regions";
    public $timestamps = false;
    protected $fillable = array('id', 'name','deleted');
    public function locality()
    {
      return $this->hasMany('App\Locality');
    }
}
