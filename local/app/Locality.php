<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locality extends Model
{
    protected $table="locality";
    public $timestamps = false;
    protected $fillable = array('id','id_region','name','deleted');
    public function region()
    {
      return $this->belongsTo('App\Region','id_region');
    }
    public function command()
    {
      return $this->hasMany('App\Commands');
    }
}
