<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Command extends Model
{

    public $timestamps = false;
    protected $table="commands";
    protected $fillable = array('id', 'id_locality','id_user','id_type','date','quantity');
    public function locality()
    {
       return $this->belongsTo('App\Locality','id_locality');
    }
    public function user()
    {
      return $this->belongsTo('App\User','id_user');
    }
}
