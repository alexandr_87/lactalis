<?php

namespace App\Providers;
use  App\Command;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

       $count_command=Command::select('id','date','id_user','id_locality','quantity','id_type')->count();

       view()->share('count_command', $count_command);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
