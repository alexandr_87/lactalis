<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});


Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::get('location',function()
    {
      return view('commands.location');
    });
    Route::post('search','CommandsController@search');
    Route::get('/home', 'HomeController@index');
    Route::get('list', 'CommandsController@index');
    Route::get('add', 'CommandsController@add');
    Route::put('save','CommandsController@save');
    Route::get('export','CommandsController@export');

    Route::get('list_localities', 'LocalitiesController@index');
    Route::get('add_locality', 'LocalitiesController@add');
    Route::put('save_locality', 'LocalitiesController@save');
    Route::get('delete_locality/{id}', ['as' => 'delete','uses' =>'LocalitiesController@destroy']);

    Route::get('list_users', 'UsersController@index');
    Route::get('add_user', 'UsersController@add');
    Route::put('save_user', 'UsersController@save');
    Route::get('delete_user/{id}', ['as' => 'delete','uses' =>'UsersController@destroy']);

    Route::get('list_regions', 'RegionsController@index');
    Route::get('add_region', 'RegionsController@add');
    Route::put('save_region', 'RegionsController@save');
    Route::get('delete_region/{id}', ['as' => 'delete','uses' =>'RegionsController@destroy']);

    Route::get('getLocalityByRegion','CommandsController@getLocalityByRegion');
    Route::get('delete/{id}', ['as' => 'delete','uses' =>'CommandsController@destroy']);
});
