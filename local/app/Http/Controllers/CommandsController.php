<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
//use Illuminate\Support\Facades\Request;
use Excel;
use App\Type;
use App\Region;
use App\Locality;
use App\User;
use App\Command;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;

class CommandsController extends Controller
{
    public function index()
    {
      if(Auth::check())
      {
        $localities=Locality::select('id','id_region','name','deleted')->where('deleted','=','0')->get();
        $regions=Region::select('id','name','deleted')->where('deleted','=','0')->get();
        $types=Type::select('id','name')->get();
        $users=User::select('id','name')->where('deleted','=','0')->get();
        if (Auth::user()->role->name=="admin")
        {
          $commands=Command::select('id','date','id_user','id_locality','quantity','id_type')->orderBy('date')->paginate(20);
          $count_command=Command::select('id','date','id_user','id_locality','quantity','id_type')->count();
        }
        else {
          $commands=Command::select('id','date','id_user','id_locality','quantity','id_type')->where('date','=',date("Y-m-d"))
          ->where('id_user','=',Auth::user()->id)->orderBy('date')->paginate(20);
          $count_command=Command::select('id','date','id_user','id_locality','quantity','id_type')->where('date','=',date("Y-m-d"))->where('id_user','=',Auth::user()->id)->count();
        }

        return view('commands.index',compact('types','regions','localities','commands','count_command','users'));
      }
      else {
        return view('auth.login');
      }
    }
    public function add()
    {
      if(Auth::check())
      {
        $localities=Locality::select('id','id_region','name','deleted')->where('deleted','=','0')->get();
        $regions=Region::select('id','name','deleted')->where('deleted','=','0')->get();
        $types=Type::select('id','name')->get();
        return view('commands.add',compact('types','regions','localities'));
      }
      else {
        return view('auth.login');
      }
    }

    public function export()
    {
        Excel::create('comenzi',function($excel)
        {
            $excel->sheet('Excel sheet', function($sheet) {

                $sheet->setOrientation('landscape');
                $commands=Command::select('id','date','id_user','id_locality','quantity','id_type')->get();
                $sheet->loadView('commands.index0',compact('commands'));
            /*    $new_arr=array();
                $i=0;
                foreach ($commands as $command) {
                  $new_arr[$i]['id']=$command->id;
                  $new_arr[$i]['data']=$command->date;
                  $new_arr[$i]['region']=$command->locality->region->name;
                  $new_arr[$i]['localitatea']=$command->locality->name;
                  $new_arr[$i]['cantitatea']=$command->quantity;
                  $new_arr[$i]['sofer']=$command->user->name;
                  $i++;
                }
                $sheet->fromArray($new_arr);*/

            });
        })->export('xls');
    }

    public function getLocalityByRegion()
    {

        $localities=Locality::select('id','id_region','name','deleted')->where('deleted','=','0')->where('id_region','=',Input::get('val'))->get();
        return $localities->toJson();
    }
    public function save()
    {

         Command::create(array(
           'id_locality'=>Input::get('localities'),
           'quantity'=>Input::get('quantity'),
           'id_type'=>Input::get('types'),
           'date'=>date('Y-m-d'),
           'id_user'=>Auth::user()->id
         ));
         //return view('commands.index',compact('types','regions','localities','commands'));
         return index();
    }

    public function destroy($id)
   {
     $new = Command::find($id);
     $new->delete();
     return Redirect::back();
   }

   public function search(Request $request)
   {
     $localities=Locality::select('id','id_region','name','deleted')->where('deleted','=','0')->get();
     $regions=Region::select('id','name','deleted')->where('deleted','=','0')->get();
     $types=Type::select('id','name')->get();
     $users=User::select('id','name')->where('deleted','=','0')->get();
     $commands=Command::select('id','date','id_user','id_locality','quantity','id_type');
     $count_command=Command::select('id','date','id_user','id_locality','quantity','id_type')->count();
     $find_user=$request->get('users');
     $find_region=$request->get('regions');
     $find_data_end=$request->get('date_end');
     $find_data_start=$request->get('date_start');
     if ($find_user!=="all")
     {
       $commands=$commands->where('id_user','=',"$find_user");
     }
     if ($find_data_start!=="" && $find_data_end!=="")
     {
       $commands=$commands->whereBetween('date', array($find_data_start, $find_data_end));
     }
     $commands=$commands->get();
     $i=0;
     $new_arr=array();
     foreach ($commands as $command) {
       if ($find_region!=="all")
       {
         if((int)$command->locality->region->id==(int)$find_region)
         {
           $new_arr[$i]['id']=$command->id;
           $new_arr[$i]['data']=$command->date;
           $new_arr[$i]['region']=$command->locality->region->name;
           $new_arr[$i]['localitatea']=$command->locality->name;
           $new_arr[$i]['cantitatea']=$command->quantity;
           $new_arr[$i]['sofer']=$command->user->name;
           $i++;
         }
       }
       else {
         $new_arr[$i]['id']=$command->id;
         $new_arr[$i]['data']=$command->date;
         $new_arr[$i]['region']=$command->locality->region->name;
         $new_arr[$i]['localitatea']=$command->locality->name;
         $new_arr[$i]['cantitatea']=$command->quantity;
         $new_arr[$i]['sofer']=$command->user->name;
         $i++;
       }

     }

     return $new_arr;
   }
}
