<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Region;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RegionsController extends Controller
{
  public function index()
  {
    $regions=Region::select('id','name','deleted')->where('deleted','=','0')->paginate(5);
    return view('region.index',compact('regions'));
  }
  public function add()
  {
    return view("region.add");
  }
  public function save()
  {
    Region::create(array(
      'name'=>Input::get('name')
    ));
    return Redirect::to('list_regions');
  }
  public function destroy($id)
 {
   $new = Region::find($id)->update(['deleted'=>'1']);
   return Redirect::back();
 }
}
