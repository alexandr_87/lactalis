<?php

namespace App\Http\Controllers;
use App\User;
use App\Command;
use App\Region;
use App\Locality;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::select("id")->where("deleted","=","0")->count();
        $regions=Region::select("id")->where("deleted","=","0")->count();
        $localities=Locality::select("id")->where("deleted","=","0")->count();
        $commands=Command::select("id")->count();
        return view('home',compact("users","commands","regions","localities"));
    }
}
