<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Locality;
use App\Region;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class LocalitiesController extends Controller
{
  public function index()
  {

    $localities=Locality::select('id','id_region','name','deleted')->where('deleted','=','0')->paginate(7);
    return view('locality.index',compact('localities'));
  }
  public function add()
  {
    $regions=Region::select('id','name','deleted')->where('deleted','=','0')->get();
    return view("locality.add",compact('regions'));
  }
  public function save()
  {
    Locality::create(array(
      'name'=>Input::get('name'),
      'id_region'=>Input::get('regions')
    ));
    return Redirect::to('list_localities');
  }
  public function destroy($id)
 {
   $new = Locality::find($id)->update([
          'deleted'=>'1']);
   return Redirect::back();
 }
}
