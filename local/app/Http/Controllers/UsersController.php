<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function index()
    {
      $users=User::select('id','name','id_role','deleted')->where('deleted','=','0')->paginate(10);
      return view("user.index",compact('users'));
    }
    public function add()
    {
      $roles=Role::select('id','name')->get();
      return view("user.add",compact('roles'));
    }

    public function save()
    {
      User::create(array(
        'name'=>Input::get('name'),
        'password'=>bcrypt(Input::get('password')),
        'id_role'=>Input::get('roles')
      ));
      return Redirect::to('list_users');
    }
    public function destroy($id)
   {
     $new = User::find($id)->update(['deleted'=>'1']);
     return Redirect::back();
   }

}
