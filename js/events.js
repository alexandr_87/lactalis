function loadLocalitiesByRegion()
{
	var val = document.getElementById('regions').value;
	var url = 'getLocalityByRegion';
	$.ajax({
		method: 'get',
		url: url,
		dataType: 'json',
		data:{val:val},
		success: function(resp){
			$('#localities').empty();
			console.log(resp);
			$.each(resp,function(key, value)
				{
					$('<option>').val(value['id']).text(value['name']).appendTo('#localities');
				});
			}
			});
}
$(function ($) {
	$('#regions').change(function()
	{
		loadLocalitiesByRegion();
	});
});
$(function(){
	$( '#search_form' ).submit(function(e) {
			e.preventDefault();
			var $form = $( this ),
      dataFrom = $form.serialize();
			$.ajax({
					type: "post",
					data: dataFrom,
					url: 'search',
					beforeSend: function(request) {
        		return request.setRequestHeader('X-CSRF-Token', $("meta[name='_token']").attr('content'));
    			},
					success: function(resp){
						$('#commands tbody').remove();
						for (i=0;i<resp.length;i++)
						{
							$('#commands').append('<tr data-toggle="collapse" data-target="#demo'+i+'" class="accordion-toggle"><td>'+resp[i]['id']+'</td><td>'+resp[i]['region']+'</td><td>'+resp[i]['localitatea']+
							'</td><td>'+resp[i]['sofer']+'</td><td>'+dateFormat(data(resp[i]['data']),'d-m-Y')+'</td><td>'+resp[i]['cantitatea']+
							' L</td></tr>'+
							'  <tr>'+
		          '    <td colspan="6" class="hiddenRow">'+
		          '      <div id="demo'+i+'" class="accordian-body collapse">'+
		          '        <a href="delete/'+resp[i]['id']+'" id="btn_delete" class= "btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></a>'+
		          '      </div>'+
		          '    </td>'+
		          '  </tr>');
						}
					}
			});

	});
});
function datePicker()
{
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

	var checkin = $('#dpd1').datepicker({
	  onRender: function(date) {
	    return date.valueOf() < now.valueOf() ? 'disabled' : '';
	  }
	}).on('changeDate', function(ev) {
	  if (ev.date.valueOf() > checkout.date.valueOf()) {
	    var newDate = new Date(ev.date)
	    newDate.setDate(newDate.getDate() + 1);
	    checkout.setValue(newDate);
	  }
	  checkin.hide();
	  $('#dpd2')[0].focus();
	}).data('datepicker');
	var checkout = $('#dpd2').datepicker({
	  onRender: function(date) {
	    return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
	  }
	}).on('changeDate', function(ev) {
	  checkout.hide();
	}).data('datepicker');
};
$(document).ready(function(){
		//
	/*	datePicker();
		$('.datepicker').datepicker({
		    format: 'mm/dd/yyyy',
		    startDate: '-3d'
		});*/

		$('tr').click(function () {
			 var sender = $(this);
			 var targetId = $(sender.attr("data-target"))
			 targetId.toggle().collapse();
	 });
    $('.filterable .btn-filter').click(function(){
        var $panel = $(this).parents('.filterable'),
        $filters = $panel.find('.filters input'),
        $tbody = $panel.find('.table tbody');
        if ($filters.prop('disabled') == true) {
            $filters.prop('disabled', false);
            $filters.first().focus();
        } else {
            $filters.val('').prop('disabled', true);
            $tbody.find('.no-result').remove();
            $tbody.find('tr').show();
        }
    });

    $('.filterable .filters input').keyup(function(e){
        /* Ignore tab key */
        var code = e.keyCode || e.which;
        if (code == '9') return;
        /* Useful DOM data and selectors */
        var $input = $(this),
        inputContent = $input.val().toLowerCase(),
        $panel = $input.parents('.filterable'),
        column = $panel.find('.filters th').index($input.parents('th')),
        $table = $panel.find('.table'),
        $rows = $table.find('tbody tr');

        var $filteredRows = $rows.filter(function(){
            var value = $(this).find('td').eq(column).text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        /* Clean previous no-result if exist */
        $table.find('tbody .no-result').remove();
        /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
        $rows.show();
        $filteredRows.hide();
        /* Prepend no-result row if all rows are filtered */
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
        }
			});

			$('.btn-danger').on('click', function() {
			    var choice = confirm('Sunteti siguri ca doriti sa stergeti acest rind?');
			    if(choice === true) {
			        return true;
			    }
			    return false;
			});
			loadLocalitiesByRegion();
    });
